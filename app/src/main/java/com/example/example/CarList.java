package com.example.example;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Objects;

public class CarList extends AppCompatActivity {
    private long lastClickTime = 0;

    final String LOG_TAG = "mLog";
    private String mark,vin,time,id,video_status, model;

    ArrayList<CarCardItem> carCardItem = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_list);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return;
                }

                Intent intent = new Intent(CarList.this, CarInfomation.class);
                startActivity(intent);
                lastClickTime = SystemClock.elapsedRealtime();
            }
        });

        DBHelperCar DBHelperCar = new DBHelperCar(this);
        SQLiteDatabase Db = DBHelperCar.getWritableDatabase();
        //DBHelperCar.onUpgrade(Db,2,3);
        Cursor cursor = Db.rawQuery("SELECT * FROM car WHERE "+DBHelperCar.KEY_INSPECTION_STATUS+" < 3 ", null);
        cursor.moveToFirst();

//Пробегаем по всем клиентам
        while (!cursor.isAfterLast()) {

            int status_ColIndex = cursor.getColumnIndex(DBHelperCar.KEY_INSPECTION_STATUS);
            video_status = cursor.getString(status_ColIndex);

            //if (video_status == "1") {
            int id_ColIndex = cursor.getColumnIndex(DBHelperCar.KEY_ID);
            int mark_ColIndex = cursor.getColumnIndex(DBHelperCar.KEY_MARK);
            int model_ColIndex = cursor.getColumnIndex(DBHelperCar.KEY_MODEL);
            int vin_ColIndex = cursor.getColumnIndex(DBHelperCar.KEY_VIN);
            int time_ColIndex = cursor.getColumnIndex(DBHelperCar.KEY_TIME);

            id = cursor.getString(id_ColIndex);
            mark = cursor.getString(mark_ColIndex);
            model= cursor.getString(model_ColIndex);
            time = cursor.getString(time_ColIndex);

            Log.d("mLog", "ID = " + id +
                    ", mark = " + mark +
                    ", model = " + model +
                    ", time_create = " + time+
                    ", video_status = " + video_status);
            carCardItem.add(new CarCardItem(id, mark, model, time, video_status));
            //Переходим к следующему клиенту
            // }
            //Переходим к следующему клиенту
            cursor.moveToNext();
        }
        cursor.close();

        ListView listViewItem =(ListView) findViewById(R.id.listVewCarItem);
        CarCardAdapter Adapter = new CarCardAdapter(this, carCardItem);
        listViewItem.setAdapter(Adapter);

    }
}