package com.example.example;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

public class CarCardAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater layoutInflater;
    ArrayList<CarCardItem> objects;
    DBHelperCar dbHelperCar;

    public CarCardAdapter(Context context, ArrayList<CarCardItem> products){
        ctx = context;
        objects =products;
        layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null){
            view = layoutInflater.inflate(R.layout.adapter_car_card,parent,false);
        }
        final CarCardItem item = getCarCardAdapter(position);
        ((TextView) view.findViewById(R.id.TextViewAdapterMarModel)).setText(item.mark);
        ((TextView) view.findViewById(R.id.TextViewAdapterTime)).setText(item.time);

        dbHelperCar = new DBHelperCar(ctx);
        final SQLiteDatabase db = dbHelperCar.getWritableDatabase();

        ImageButton imageButtonAdapterDelete = (ImageButton)view.findViewById(R.id.imageButtonAdapterDelete);

        imageButtonAdapterDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openQuitDialog(db,position, item);
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = (new Intent(ctx, CarCard.class));
                intent.putExtra("Num_osm", item.id);
                ctx.startActivity(intent);
                //ctx.startActivities(new Intent[]{new Intent(ctx, Osmotr_1.class)});
                Log.d("mLog", "Тип зашел посмотреть данные ID= "+item.id+" ;");
            }
        });
        return view;
    }

    CarCardItem getCarCardAdapter(int position) {
        return ((CarCardItem) getItem(position));
    }
    public void remove(int position){
        objects.remove(objects.get(position));
    }

    private void openQuitDialog(final SQLiteDatabase db, final int position, final CarCardItem item)
    {
        AlertDialog.Builder quitDialog = new AlertDialog.Builder(ctx);
        quitDialog.setTitle("Удалить: Вы уверены?");

        quitDialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

                Cursor c = db.query(DBHelperCar.TABLE_CONTACTS, null, null, null, null, null, null);
                Log.d("mLog", "Тип удалил");
                objects.remove(position); //or some other task
                notifyDataSetChanged();
                int delCount = db.delete(DBHelperCar.TABLE_CONTACTS, DBHelperCar.KEY_ID + "= ?", new String[] {item.id});
                Log.d("mLog", "deleted rows count = " + delCount);
                Toast.makeText(ctx, "Тип удалил", Toast.LENGTH_SHORT).show();

            }

        });

        quitDialog.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
            }
        });

        quitDialog.show();
    }
}
