package com.example.example;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class CarBody extends AppCompatActivity implements View.OnClickListener {
    String Status = "clean";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.clean);

        ImageButton  ImageButtonLFFlank = (ImageButton) findViewById(R.id.imageButtonLFFlank);
        ImageButton  ImageButtonFront = (ImageButton) findViewById(R.id.imageButtonFront);
        ImageButton  ImageButtonRFFlank = (ImageButton) findViewById(R.id.imageButtonRFFlank);
        ImageButton  ImageButtonHood = (ImageButton) findViewById(R.id.imageButtonHood);
        ImageButton  ImageButtonLDoor = (ImageButton) findViewById(R.id.imageButtonLDoor);
        ImageButton  ImageButtonRDoor = (ImageButton) findViewById(R.id.imageButtonRDoor);
        ImageButton  ImageButtonBack = (ImageButton) findViewById(R.id.imageButtonBack);
        ImageButton  ImageButtonLBFlank = (ImageButton) findViewById(R.id.imageButtonLBFlank);
        ImageButton  ImageButtonBackFront = (ImageButton) findViewById(R.id.imageButtonBackFront);
        ImageButton  ImageButtonRBFlank = (ImageButton) findViewById(R.id.imageButtonRBFlank);
        ImageButton  ImageButtonElement = (ImageButton) findViewById(R.id.imageButtonElement);

        ImageButtonLFFlank.setOnClickListener(this);
        ImageButtonLFFlank.setOnClickListener(this);
        ImageButtonFront.setOnClickListener(this);
        ImageButtonRFFlank.setOnClickListener(this);
        ImageButtonHood.setOnClickListener(this);
        ImageButtonLDoor.setOnClickListener(this);
        ImageButtonRDoor.setOnClickListener(this);
        ImageButtonBack.setOnClickListener(this);
        ImageButtonLBFlank.setOnClickListener(this);
        ImageButtonBackFront.setOnClickListener(this);
        ImageButtonRBFlank.setOnClickListener(this);
        ImageButtonElement.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        TextView textView = (TextView) findViewById(R.id.textView);
        ImageButton button =(ImageButton) findViewById(R.id.imageButtonElement);


        switch (view.getId()) {
            case R.id.imageButtonLFFlank:
                Status = "lfflank";
                break;
            case R.id.imageButtonFront:
                Status = "front";
                break;
            case R.id.imageButtonRFFlank:
                Status = "rfflank";
                break;
            case R.id.imageButtonHood:
                Status = "hood";
                break;
            case R.id.imageButtonLDoor:
                Status = "ldoor";
                break;
            case R.id.imageButtonRDoor:
                Status = "rdoor";
                break;
            case R.id.imageButtonBack:
                Status = "back";
                break;
            case R.id.imageButtonLBFlank:
                Status = "lbflank";
                break;
            case R.id.imageButtonBackFront:
                Status = "backfront";
                break;
            case R.id.imageButtonRBFlank:
                Status = "rbflank";
                break;
            case R.id.imageButtonElement:
                Intent intent = new Intent(this, CarElement.class);
                intent.putExtra("Status", Status);
                startActivity(intent);
                break;
        }
        imageView.setImageResource(getImageId(this, Status));
        textView.setText(getStringId(this, Status));
    }
    public static int getImageId(Context context, String imageName) {
        return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
    }
    public static int getStringId(Context context, String stringName) {
        return context.getResources().getIdentifier("string/" + stringName, null, context.getPackageName());
    }
}