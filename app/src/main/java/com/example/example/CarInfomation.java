package com.example.example;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import ir.mirrajabi.searchdialog.core.Searchable;

public class CarInfomation extends AppCompatActivity {
    private long lastClickTime = 0;

    private DBHelperCar dbHelperCar;
    private DBHelper mDBHelper;
    private SQLiteDatabase mDb;

    EditText EditTextUserMark;
    EditText EditTextUserModel;
    EditText EditTextUserGen;
    EditText EditTextUserTypeBody;
    EditText EditTextUserModification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_infomation);;

        mDBHelper = new DBHelper(this);
        try {
            mDBHelper.updateDataBase();
        } catch (IOException mIOException) {
            throw new Error("UnableToUpdateDatabase");
        }

        try {
            mDb = mDBHelper.getWritableDatabase();
        } catch (SQLException mSQLException) {
            throw mSQLException;
        }

        EditTextUserMark = (EditText) findViewById(R.id.EditTextUserMark);
        EditTextUserModel = (EditText) findViewById(R.id.EditTextUserModel);
        EditTextUserGen = (EditText) findViewById(R.id.EditTextUserGen);
        EditTextUserTypeBody = (EditText) findViewById(R.id.EditTextUserTypeBody);
        EditTextUserModification = (EditText) findViewById(R.id.EditTextUserModification);

        EditTextUserMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String text_1 = "SELECT DISTINCT * FROM car_mark";

                new SimpleSearchDialogCompat(CarInfomation.this, "Search..", "What are you looking forr..?",
                        null, initdata(text_1, 1), new SearchResultListener<Searchable>() {
                    @Override
                    public void onSelected(BaseSearchDialogCompat baseSearchDialogCompat, Searchable searchable, int i) {

                        EditTextUserMark.setText(searchable.getTitle());
                        EditTextUserModel.setText("");
                        EditTextUserGen.setText("");
                        EditTextUserTypeBody.setText("");
                        EditTextUserModification.setText("");

                        baseSearchDialogCompat.dismiss();
                    }
                }).show();
            }
        });
        EditTextUserModel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (EditTextUserMark.getText().toString().equals("")) {
                    openDialog(R.string.mark_emply);
                } else {
                    String text_2 =
                            "SELECT car_model.name FROM car_model, car_mark WHERE car_model.id_car_mark = car_mark.id_car_mark AND car_mark.name = '" + EditTextUserMark.getText().toString() + "'";
                    new SimpleSearchDialogCompat(CarInfomation.this, "Search..", "What are you looking forr..?",
                            null, initdata(text_2, 0), new SearchResultListener<Searchable>() {
                        @Override
                        public void onSelected(BaseSearchDialogCompat baseSearchDialogCompat, Searchable searchable, int i) {
                            EditTextUserModel.setText(searchable.getTitle());
                            EditTextUserGen.setText("");
                            EditTextUserTypeBody.setText("");
                            EditTextUserModification.setText("");
                            baseSearchDialogCompat.dismiss();
                        }
                    }).show();
                }
            }
        });

        EditTextUserGen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    if (EditTextUserModel.getText().toString().equals("")) {
                        openDialog(R.string.gen_emply);
                    } else {
                        String text_3 = "SELECT car_generation.name FROM car_generation, car_model,car_mark " +
                                "WHERE car_generation.id_car_model = car_model.id_car_model " +
                                "AND car_model.id_car_mark = car_mark.id_car_mark AND car_mark.name = '" + EditTextUserMark.getText().toString()
                                + "' AND car_model.name='" + EditTextUserModel.getText().toString() + "'";
                        new SimpleSearchDialogCompat(CarInfomation.this, "Search..", "What are you looking forr..?",
                                null, initdata(text_3, 0), new SearchResultListener<Searchable>() {
                            @Override
                            public void onSelected(BaseSearchDialogCompat baseSearchDialogCompat, Searchable searchable, int i) {
                                EditTextUserGen.setText(searchable.getTitle());
                                EditTextUserTypeBody.setText("");
                                EditTextUserModification.setText("");
                                baseSearchDialogCompat.dismiss();
                            }
                        }).show();
                    }
                }
            });
        EditTextUserTypeBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    if (EditTextUserGen.getText().toString().equals("")) {
                        openDialog(R.string.type_kuzov_emply);
                    } else {
                        String text_4 = "SELECT car_serie.name FROM car_serie,car_generation, car_model,car_mark " +
                                "WHERE car_generation.id_car_model = car_model.id_car_model AND car_model.id_car_mark = car_mark.id_car_mark " +
                                "AND  car_generation.id_car_generation = car_serie.id_car_generation "
                                + "AND car_mark.name = '" + EditTextUserMark.getText().toString()
                                + "' AND car_model.name='" + EditTextUserModel.getText().toString()
                                + "' AND car_generation.name = '" + EditTextUserGen.getText().toString() + "'";

                        new SimpleSearchDialogCompat(CarInfomation.this, "Search..", "What are you looking forr..?",
                                null, initdata(text_4, 0), new SearchResultListener<Searchable>() {
                            @Override
                            public void onSelected(BaseSearchDialogCompat baseSearchDialogCompat, Searchable searchable, int i) {
                                EditTextUserTypeBody.setText(searchable.getTitle());
                                EditTextUserModification.setText("");
                                baseSearchDialogCompat.dismiss();
                            }
                        }).show();
                    }
                }
            });

        EditTextUserModification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    if (EditTextUserTypeBody.getText().toString().equals("")) {
                        openDialog(R.string.modif_emply);
                    } else {
                        String text_5 = "SELECT car_modification.name FROM car_modification,car_serie,car_generation, car_model,car_mark" +
                                " WHERE car_generation.id_car_model = car_model.id_car_model AND car_model.id_car_mark = car_mark.id_car_mark " +
                                "AND  car_generation.id_car_generation = car_serie.id_car_generation AND car_serie.id_car_serie = car_modification.id_car_serie "
                                + "AND car_mark.name = '" + EditTextUserMark.getText().toString()
                                + "' AND car_model.name='" + EditTextUserModel.getText().toString()
                                + "' AND car_generation.name = '" + EditTextUserGen.getText().toString()
                                + "' AND car_serie.name = '" + EditTextUserTypeBody.getText().toString() + "'";

                        new SimpleSearchDialogCompat(CarInfomation.this, "Search..", "What are you looking forr..?",
                                null, initdata(text_5, 0), new SearchResultListener<Searchable>() {
                            @Override
                            public void onSelected(BaseSearchDialogCompat baseSearchDialogCompat, Searchable searchable, int i) {
                                EditTextUserModification.setText(searchable.getTitle());
                                baseSearchDialogCompat.dismiss();
                            }
                        }).show();
                    }
                }
            });

        Button Next = (Button) findViewById(R.id.ButtonCarBody);
        Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
                    return;
                }

                if (EditTextUserMark.getText().toString().isEmpty() || EditTextUserModel.getText().toString().isEmpty() || EditTextUserGen.getText().toString().isEmpty()
                        || EditTextUserModification.getText().toString().isEmpty()) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(CarInfomation.this);
                    builder.setTitle(R.string.app_name)
                            .setMessage("Заполните все поля")
                            .setCancelable(false)
                            .setNegativeButton("ОК",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {

                    Intent intent = new Intent(CarInfomation.this, CarBody.class);
                    dbHelperCar = new DBHelperCar(CarInfomation.this);
                    ContentValues cv = new ContentValues();
                    // получаем данные из полей ввода

                    // подключаемся к БД
                    SQLiteDatabase db = dbHelperCar.getWritableDatabase();

                    String user_mark = EditTextUserMark.getText().toString();
                    String user_model = EditTextUserModel.getText().toString();
                    String user_type_body = EditTextUserTypeBody.getText().toString();
                    String user_gen = EditTextUserGen.getText().toString();
                    String user_modif = EditTextUserModification.getText().toString();

                    long date = System.currentTimeMillis();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss.SS");
                    String user_time_create = sdf.format(date);

                    String user_video = Integer.toString(DBHelperCar.VIDEO_STATUS_NOT_EXISTS);

                    cv.put(dbHelperCar.KEY_MARK, user_mark);
                    cv.put(dbHelperCar.KEY_MODEL, user_model);
                    cv.put(dbHelperCar.KEY_TYPE_KUZOV, user_type_body);
                    cv.put(dbHelperCar.KEY_GEN, user_gen);
                    cv.put(dbHelperCar.KEY_MODIF, user_modif);

                    cv.put(dbHelperCar.KEY_INSPECTION_STATUS, user_video);

                    cv.put(dbHelperCar.KEY_TIME, user_time_create);

                    db.insert(DBHelperCar.TABLE_CONTACTS, null, cv);

                    Log.d("mLog", "Add new rows");
                    cv.clear();
                    Cursor c = db.query(DBHelperCar.TABLE_CONTACTS, null, null, null, null, null, null);

                    if (c.moveToFirst()) {
                        c.moveToLast();
                        int idColIndex = c.getColumnIndex(DBHelperCar.KEY_ID);
                    } else Log.d("mLog", "No rows, this id emply");

                    c.close();
                    dbHelperCar.close();
                    startActivity(intent);
                }

                lastClickTime = SystemClock.elapsedRealtime();
            }

        });

    }

    private ArrayList<SearchModel> initdata(String t, int k){
        ArrayList<SearchModel> items = new ArrayList<>();

        Cursor cursor = mDb.rawQuery(t, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            items.add(new SearchModel(cursor.getString(k)));

            cursor.moveToNext();
        }
        cursor.close();
        return items;
    }

    public void openDialog(int text) {
        final Dialog dialog = new Dialog(CarInfomation.this);
        dialog.setContentView(R.layout.dialog_emply_info_person);
        Button button = (Button) dialog.findViewById(R.id.button);
        TextView textView = (TextView) dialog.findViewById(R.id.text_error_emply_info_person);
        textView.setText(text);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }
}