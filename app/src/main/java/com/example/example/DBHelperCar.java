package com.example.example;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class DBHelperCar extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME ="CAR_DB";
    public static final String TABLE_CONTACTS ="car";

    public static final String KEY_ID ="_id";
    public static final String KEY_MARK ="mark";
    public static final String KEY_MODEL ="model";
    public static final String KEY_TYPE_KUZOV ="type";
    public static final String KEY_GEN ="gen";
    public static final String KEY_MODIF ="modif";
    public static final String KEY_GOS ="gos";
    public static final String KEY_VIN ="vin";
    public static final String KEY_INSPECTION_STATUS ="inspection_status";
    public static final String KEY_TIME ="time";

    public static final int VIDEO_STATUS_UNDEFINED = 0;//Основа , всегда при создвние
    public static final int VIDEO_STATUS_NOT_EXISTS = 1;//Есть данные, но видео не отснято
    public static final int VIDEO_STATUS_EXISTS = 2;//Есть данные и видео, но видео не отправленно
    public static final int VIDEO_STATUS_SEND = 3;//Есть данные и видео и все оправленно
    public static final int VIDEO_STATUS_COMPLIT = 4;//осмотр завершён успешно
    public static final int VIDEO_STATUS_BAD = 5;//осмотр завершён не успешно
    public static final int VIDEO_STATUS_DELETED = 6;//осмотр удалён



    public DBHelperCar(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_CONTACTS + "( " + KEY_ID + " integer primary key AUTOINCREMENT,"+
                KEY_MARK +" text,"+KEY_MODEL +" text,"+KEY_TYPE_KUZOV +" text,"+KEY_GEN +" text,"+KEY_MODIF +" text," +
                KEY_GOS +" text," + KEY_INSPECTION_STATUS +" text,"+KEY_VIN +" text,"+KEY_TIME +" text"+")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_CONTACTS);

        onCreate(db);
    }
    public void updateDataBase() throws IOException {

    }

}
