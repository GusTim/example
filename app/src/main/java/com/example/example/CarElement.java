package com.example.example;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class CarElement extends AppCompatActivity {
    String Status;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_element_car);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Status = getIntent().getStringExtra("Status");
        CarBody main = new CarBody();
        getSupportActionBar().setTitle(main.getStringId(this, Status));

        ImageView ElementImage =(ImageView) findViewById(R.id.imageViewElement);
        String ImageElementName = Status+"_photo";
        ElementImage.setImageResource(main.getImageId(this, ImageElementName));

        Button button = (Button) findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CarElement.this, CarList.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}